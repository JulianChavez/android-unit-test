package co.gov.metromedellin.appCivicanew.dev

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.example.unittest.R
import com.example.unittest.databinding.FragmentHomeBindingImpl
import com.example.unittest.screens.login.viewmodel.LoginViewModel

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBindingImpl
    private val loginViewModel: LoginViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.lifecycleOwner = this
        binding.loginViewModel = loginViewModel

        return binding.root
    }

}