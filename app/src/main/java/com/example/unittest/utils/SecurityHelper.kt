package com.example.unittest.utils

import android.util.Base64
import com.valid.security.entities.TransformAes
import com.valid.security.entities.TransformRsa
import com.valid.security.helpers.KeyHelper
import com.valid.security.managers.AesCrypto
import com.valid.security.managers.RsaCrypto
import com.valid.security.managers.VsshCryptoManager
import com.valid.security.utils.SecurityUtils
import com.valid.utils.VsshLogger
import com.valid.vssh_android_core.utils.CoreSecurityHelper
import com.valid.vssh_android_core.utils.EncodeHelper
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

object SecurityHelper {
    private val TAG = SecurityHelper.javaClass.simpleName

    fun encryptAES(value: String?, key: String?): String {
        val key: SecretKey? =
            SecretKeySpec(
                SecurityUtils.getByteArrayFromHexString(key),
                "AES"
            )
        val securityAes: VsshCryptoManager<TransformAes> = AesCrypto()
        return SecurityUtils.getHexStringFromByteArray(
            securityAes.encrypt(TransformAes.GCM, value, key)
        )
    }

    fun decryptAES(value: String, key: String): String {
        val key: SecretKey? =
            SecretKeySpec(
                SecurityUtils.getByteArrayFromHexString(key),
                "AES"
            )
        val securityAes: VsshCryptoManager<TransformAes> = AesCrypto()
        return String(
            securityAes.decrypt(TransformAes.GCM, value, key)
        )
    }

    fun encryptRSAToLogin(value: String, key: String): String {
        val securityHelper = CoreSecurityHelper()
        return securityHelper.encryptRsa(
            EncodeHelper.getHexSha256FromString(
                value
            ),
            KeyHelper.getPublicKeyFromString(
                KeyHelper.buildPublicKeyAsString(
                    key
                )
            )
        )

    }

    fun encryptRSA(value: String, key: String): String {
        val publicKey = buildPublicKeyFromString(key)
        val securityRsa: VsshCryptoManager<TransformRsa> = RsaCrypto()
        val byteArray = securityRsa.encrypt(TransformRsa.PKCS1, value, publicKey)
        return SecurityUtils.getHexStringFromByteArray(byteArray)
    }

    private fun buildPublicKeyFromString(key: String): PublicKey? {
        try {
            val byte: ByteArray = Base64.decode(key, Base64.DEFAULT)
            val x509EncodedKeySpec = X509EncodedKeySpec(byte)
            val factory: KeyFactory = KeyFactory.getInstance("RSA")
            return factory.generatePublic(x509EncodedKeySpec)
        } catch (e: Exception) {
            VsshLogger.logError(TAG, e.message, e)
        }
        return null
    }
}