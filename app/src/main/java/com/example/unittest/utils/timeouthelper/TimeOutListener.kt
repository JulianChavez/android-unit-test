package com.example.unittest.utils.timeouthelper

interface TimeOutListener {
    fun onLogoutSession()
}