package com.example.unittest.utils

import java.io.Serializable

data class NewDeviceArguments(
    var idType:String="",
    var idData:String=""
): Serializable