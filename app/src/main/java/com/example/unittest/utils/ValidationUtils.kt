package com.example.unittest.utils

import android.content.Context
import com.google.android.material.textfield.TextInputLayout
import java.util.*

object ValidationUtils {

    fun validatePasswordStartsWith(password: String, prefixes: List<String>): Boolean {
        for (prefix in prefixes)
            if (password.startsWith(prefix, false))
                return false
        return true
    }

    fun validateEmailAddress(emailAddress: String): Boolean =
        androidx.core.util.PatternsCompat.EMAIL_ADDRESS.matcher(emailAddress).matches()

    fun isEmptyOrBlankText(text: String): Boolean = text.isEmpty() || text.isBlank()

    fun isNullEmptyOrBlankText(text: String?): Boolean =
        text.isNullOrEmpty() || text.isNullOrBlank()

    fun validatePhoneNumber(phone: String): Boolean =
        !isEmptyOrBlankText(phone)
                && phone.length == ParamsHelper.params.phoneNumberLength
                && !validatePasswordStartsWith(phone, ParamsHelper.params.phoneNumberPrefix)

    fun validateOtpCode(value: String, length: Int = OTP_MIN_SIZE): Boolean {
        return value.length == length && !isEmptyOrBlankText(value)
    }

    private val OTP_MIN_SIZE: Int get() = ParamsHelper.params.recoverCodeLength


}