package com.example.unittest.utils

import com.example.unittest.screens.common.model.ParamsModelResponse
import com.example.unittest.screens.common.model.UserData

object ParamsHelper {
    var params = ParamsModelResponse()
    var userData = UserData()
    var fireBaseToken: String = ""
    var mapToken: String = ""
}