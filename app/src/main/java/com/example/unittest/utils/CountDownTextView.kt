package com.example.unittest.utils

import android.content.Context
import android.os.CountDownTimer
import android.widget.TextView
import androidx.annotation.StringRes
import com.example.unittest.R

class CountDownTextView(
    val time: Long = 60000L,
    val interval: Long = 1000L,
    val textView: TextView,
    val onFinish: (() -> Unit)? = null,
    val onTick: ((Long) -> Unit)? = null,
    @StringRes val endTime: Int = R.string.end_timer,
    @StringRes val timeFormat: Int = R.string.timer_format
) {
    private var countdownTimer: CountDownTimer
    private val timeConstant = 60
    private val minuteInMilliseconds = 1000
    private val context: Context by lazy { textView.rootView.context }

    init {
        countdownTimer = object : CountDownTimer(time, interval) {
            override fun onFinish() {
                textView.text = context.getString(endTime)
                onFinish?.invoke()
            }

            override fun onTick(millisUntilFinished: Long) {
                val minute = (millisUntilFinished / minuteInMilliseconds) / timeConstant
                val seconds = (millisUntilFinished / minuteInMilliseconds) % timeConstant
                val formattedMinute = timerFormatted(minute)
                val formattedSeconds = timerFormatted(seconds)
                textView.text = context.getString(timeFormat, formattedMinute, formattedSeconds)
                onTick?.invoke(millisUntilFinished)
            }
        }
    }

    private fun timerFormatted(value: Long): String =
        if (value < 10) "0$value" else value.toString()

    fun startTimer() {
        countdownTimer.start()
    }

    fun stopTimer() {
        countdownTimer.cancel()
    }
}