package com.example.unittest.utils.timeouthelper

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.example.unittest.utils.AppConstants
import java.util.*


class DatePickerFragment(val listener: (date: String) -> Unit) :
    DialogFragment(), DatePickerDialog.OnDateSetListener{

    private var actualYear: Int = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        actualYear = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val picker =DatePickerDialog(activity as Context, this, actualYear, month, day)
        picker.datePicker.maxDate = calendar.timeInMillis
        return picker
    }

    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        val date = if (actualYear - year >= 18) {
            "$day/${month + 1}/$year"
        } else {
            AppConstants.VALUE_YOUNGER
        }

        listener(date)
    }


}