package com.example.unittest.utils

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class PreferencesHelper @Inject constructor(val context: Context) {
    private val preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(
            PREF_NAME,
            MODE_PRIVATE
        )
    }

    var lastLoginAttempt: Long
        get() = preferences.getLong(LAST_TIME_LOGIN_ATTEMPT, 0L)
        set(time) {
            preferences.edit().run {
                putLong(LAST_TIME_LOGIN_ATTEMPT, time)
                apply()
            }
        }

    var stringKeyValidation: String
        get() = preferences.getString(KEY_VALIDATION, "")!!
        set(string) {
            preferences.edit().run {
                putString(KEY_VALIDATION, string)
                apply()
            }
        }

    var departments: String
        get() = preferences.getString(KEY_DEPARTMENTS, "")!!
        set(value) {
            preferences.edit().run {
                putString(KEY_DEPARTMENTS, value)
                apply()
            }
        }

    var firebaseToken: String
        get() = preferences.getString(KEY_FIREBASE, "")!!
        set(value) {
            preferences.edit().run {
                putString(KEY_FIREBASE, value)
                apply()
            }
        }

    var viewHelp: Boolean
        get() = preferences.getBoolean(KEY_HELP_VIEW, false)
        set(value) {
            preferences.edit().run {
                putBoolean(KEY_HELP_VIEW, value)
                apply()
            }
        }

    var firstInstall:Boolean
        get() = preferences.getBoolean(KEY_FIRST_INSTALL, true)
        set(value){
            preferences.edit().run {
                putBoolean(KEY_FIRST_INSTALL,value)
                apply()
            }
        }

    var subCategories: String
        get() = preferences.getString(KEY_SUBCATEGORIES, "")!!
        set(value) {
            preferences.edit().run {
                putString(KEY_SUBCATEGORIES, value)
                apply()
            }
        }

    fun removeAllPreferences(){
        preferences.edit().clear().apply()
    }

    fun getStringPreference(key: String): String? = preferences.getString(key, "")

    fun setStringPreference(key: String, value: String): Boolean {
        with(preferences.edit()) {
            putString(key, value)
            commit()
        }
        return true
    }

    companion object {
        const val PREF_NAME = "preferencesKey"
        private const val MODE_PRIVATE: Int = 0
        private const val LAST_TIME_LOGIN_ATTEMPT = "last_time_login_attempt"
        private const val KEY_VALIDATION = "key_validation"
        private const val KEY_DEPARTMENTS = "key_departments"
        private const val KEY_FIREBASE = "firebase_tk"
        private const val KEY_FIRST_INSTALL = "is_first_install"
        private const val KEY_HELP_VIEW = "key_help"
        private const val KEY_SUBCATEGORIES = "key_subcategories"
    }
}