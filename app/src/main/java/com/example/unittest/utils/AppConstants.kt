package com.example.unittest.utils

class AppConstants {

    companion object {
        const val API_GET_CERTIFICATES = "/vsshcore-server-security"
        const val API_EXCHANGE_KEY = "/vsshcore-exchange-key"

        const val TIME_SPLASH_SETUP = 5000L
        const val SECOND_MILLISECONDS = 1000L
        const val ERROR_EMPTY_MAIL_PHONE = -6
        const val ERROR_OTP_EXCEPTION_CODE = -7
        const val ERROR_EMPTY_OTP_CODE = -5
        const val SC_STATUS_OTP_IS_NOT_GENERATED = 26

        const val HIDE_EDIT_MAIL = "no_edit_mail"


        const val KEY_CUSTOMER = "customer"
        const val KEY_PHONE = "phone"
        const val KEY_EMAIL = "email"
        const val KEY_TYPE = "type"
        const val KEY_ACCEPT_ENCODING = "accept-encoding"
        const val KEY_USER_STATE = "key_user_state"
        const val KEY_TYPE_MIGRATION = "type_migration"

        const val VALUE_DEFLATE = "deflate, br"
        const val VALUE_CUSTOMER_CLIENT_HELLO = "superapp-clienthello"
        const val KEY_BIRTH_DATE = "birthDate"
        const val KEY_FIRE_BASE_TOKEN = "tokenFirebaseAuthorization"
        const val API_CLIENT_HELLO = "/vssh-login-services"
        const val VALUE_APPLICATION_CODE = "valid_vssh"
        const val VALUE_CUSTOMER_LOGIN = "superapp-login"
        const val VALUE_CUSTOMER_NEW_DEVICE = "superapp-updatedevice"
        const val VALUE_CUSTOMER_EXPIRED = "superapp-updatetokens"
        const val VALUE_OTP_PHONE_DEVICE = "state_email"
        const val VALUE_OTP_NEW_DEVICE = "new_device"
        const val VALUE_OTP_EXPIRED = "expired"
        const val VALUE_STATE_MAIL = "state_mail"
        const val VALUE_OTP_MAIL = "otp_mail"
        const val VALUE_OTP_PHONE = "otp_phone"
        const val VALUE_GENERATE_OTP = "superapp-generateotp"
        const val VALUE_VALIDATE_OTP = "superapp-validateotp"
        const val KEY_SIGN = "sign"
        const val KEY_USE_ALLIES = "use_allies"
        const val VALUE_OTP_REFRESH = "otp_refresh"
        const val KEY_AES_KEY = "aesKey"
        const val KEY_OTP_NEW_DEVICE = "verifyUser"
        const val VALUE_AES_KEY = "AES_256"
        const val KEY_VALIDATION = "Validation"
        const val KEY_AUTHORIZATION_CODE = "authorizationCode"
        const val API_AUTHENTICATION_KEY = "/metro-authentication-api"
        const val API_LOGIN = "/vssh-login-services"
        const val API_OTP_GENERATE = "/vssh-otp-service"
        const val API_OTP_VALIDATE = "/vssh-otp-service"
        const val API_ALIADOS_EXCHANGE = "v1"
        const val API_ALIADOS_AUTHORIZE = "v1"
        const val EP_REFRESH_SESSION = "/auth/refreshSession"
        const val SN_REFRESH = "refreshSession"
        const val SC_EXPIRED_SESSION = 8
        const val SC_EXPIRED_SESSION_CORE = 440
        const val STATUS_CODE_WRONG_PASSWORD = 2
        const val SC_LOGIN_LOCKED_USER = 22
        const val VALUE_CUSTOMER_GET_PARAMS = "metro-getparams"
        const val KEY_APPLICATION_CODE = "applicationCode"
        const val KEY_SO = "so"
        const val VALUE_SO = "ANDROID"
        const val VALUE_FREQUENT = "frecuentes"
        const val VALUE_FREQUENT_CODE = 6
        const val VALUE_YOUNGER = "younger"
        const val KEY_VERSION = "version"
        const val KEY_LANGUAGE = "language"
        const val VALUE_LANGUAGE = "es"
        const val API_GET_PARAMS = "/vssh-login-services"
        const val EP_PARAMS = "/auth/getParameters"
        const val KEY_DERIVATION_DATA = "derivationData"
        const val API_DERIVE = "/vsshcore-server-security"
        const val API_GET_USER_DATA = "/metro-register-api/user/get"
        const val EP_DERIVE = "/security/deriveKey"



    }
}