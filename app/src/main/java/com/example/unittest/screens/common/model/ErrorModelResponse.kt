package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class ErrorModelResponse(
    @SerializedName("errorType")
    var errorType: EnumErrorType? = EnumErrorType.CUSTOM,
    @SerializedName("errorMessage")
    var errorMessage: String? = "",
    @SerializedName("internalErrorCode")
    var internalErrorCode:Int? = 0
)