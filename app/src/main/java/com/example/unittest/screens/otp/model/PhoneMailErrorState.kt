package com.example.unittest.screens.otp.model

sealed class PhoneMailErrorState
object NoErrorState : PhoneMailErrorState()
object  PhoneErrorState : PhoneMailErrorState()
object MailErrorState : PhoneMailErrorState()