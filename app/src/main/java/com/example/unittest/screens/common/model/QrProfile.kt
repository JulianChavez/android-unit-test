package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class QrProfile(
    @SerializedName("description")
    val description: String = "",
    @SerializedName("id")
    val id: Int = 6
)