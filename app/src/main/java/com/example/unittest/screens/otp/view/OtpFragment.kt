package com.example.unittest.screens.otp.view

import android.os.Bundle
import android.text.InputFilter
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.unittest.R
import com.example.unittest.databinding.FragmentOtpBinding
import com.example.unittest.screens.common.base.BaseActivity
import com.example.unittest.screens.common.model.EnumUserState
import com.example.unittest.screens.login.view.LoginActivity
import com.example.unittest.screens.otp.model.*
import com.example.unittest.screens.otp.viewmodel.OtpViewModel
import com.example.unittest.utils.*
import com.example.unittest.utils.AppConstants.Companion.ERROR_EMPTY_MAIL_PHONE
import com.example.unittest.utils.AppConstants.Companion.ERROR_EMPTY_OTP_CODE
import com.example.unittest.utils.AppConstants.Companion.ERROR_OTP_EXCEPTION_CODE
import com.example.unittest.utils.AppConstants.Companion.KEY_EMAIL
import com.example.unittest.utils.AppConstants.Companion.KEY_TYPE_MIGRATION
import com.example.unittest.utils.AppConstants.Companion.KEY_USER_STATE
import com.example.unittest.utils.AppConstants.Companion.SECOND_MILLISECONDS
import com.valid.vssh_android_core.utils.EncodeHelper


class OtpFragment : Fragment() {

    private lateinit var binding: FragmentOtpBinding
    private val otpViewModel: OtpViewModel by activityViewModels()
    private lateinit var timer: CountDownTextView
    private var otpExpirationTime: Long =
        ParamsHelper.params.expirationTimeCode * SECOND_MILLISECONDS
    private var isSecondExpiration = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_otp, container, false)
        binding.lifecycleOwner = this
        binding.otpViewModel = otpViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (!findNavController().popBackStack())
                requireActivity().finish()
            otpViewModel.clearOtpLiveDataValues()
            binding.etOtpInput.text?.clear()
        }

        val maxLength = ParamsHelper.params.recoverCodeLength
        binding.etOtpInput.filters = arrayOf(InputFilter.LengthFilter(maxLength))

        otpViewModel.setViewType(OtpValidationViewType.REGULAR_REGISTER)

        arguments?.getString(AppConstants.VALUE_STATE_MAIL)?.let { stateEmail ->
            when (stateEmail) {
                OtpValidationViewType.RECOVER_PASS.name ->
                    otpViewModel.setViewType(OtpValidationViewType.RECOVER_PASS)
                OtpValidationViewType.ONLY_MY_FARE_MAIL.name ->
                    otpViewModel.setViewType(OtpValidationViewType.ONLY_MY_FARE_MAIL)
                OtpValidationViewType.ONLY_MY_FARE_PHONE.name ->
                    otpViewModel.setViewType(OtpValidationViewType.ONLY_MY_FARE_PHONE)
            }
        }

        arguments?.getString(AppConstants.VALUE_OTP_MAIL)?.let { email ->
            if (ValidationUtils.isEmptyOrBlankText(email)) return@let
            otpViewModel.isPhone.value = false
            otpViewModel.otpValidateType = email
//            otpViewModel.viewData = EncodeHelper.encodeEmail(email)
        }

        arguments?.getString(AppConstants.VALUE_OTP_PHONE)?.let { phone ->
            if (ValidationUtils.isEmptyOrBlankText(phone)) return@let
            otpViewModel.isPhone.value = true
            otpViewModel.otpValidateType = phone
            otpViewModel.viewData = phone
        }

        arguments?.getString(AppConstants.VALUE_OTP_REFRESH)?.let { phone ->
            if (ValidationUtils.isEmptyOrBlankText(phone)) return@let
            otpViewModel.setViewType(OtpValidationViewType.REFRESH_TOKEN)
            otpViewModel.isPhone.value = true
            otpViewModel.otpValidateType = phone
            otpViewModel.viewData = phone
        }

        arguments?.getString(AppConstants.VALUE_OTP_PHONE_DEVICE)?.let { phone ->
            otpViewModel.setViewType(OtpValidationViewType.NEW_DEVICE)
            otpViewModel.isPhone.value = true
            otpViewModel.otpValidateType = AppConstants.KEY_OTP_NEW_DEVICE
            otpViewModel.viewData = phone
            otpViewModel.showEditPhone.value =
                otpViewModel.otpValidationViewType.value == OtpValidationViewType.NEW_DEVICE
        }

        arguments?.get(AppConstants.VALUE_OTP_NEW_DEVICE)?.let { idData ->
            idData as NewDeviceArguments
            otpViewModel.idData = idData.idData
            otpViewModel.idType = idData.idType
        }

        otpViewModel.useAllies = arguments?.getBoolean(AppConstants.KEY_USE_ALLIES) ?: false

        if (otpViewModel.useAllies) {
            otpViewModel.otpValidateType = AppConstants.KEY_OTP_NEW_DEVICE
        }

        otpViewModel.callGenerateOtp()
        setupControls()
        observeViewModel()
    }

    private fun setupControls() {
        timer = CountDownTextView(
            time = otpExpirationTime,
            textView = binding.tvCounter,
            onFinish = {
                binding.btnValidateOtp.isEnabled = true
                otpViewModel.endTime.value = true
            }
        )

        timer.startTimer()

        binding.etOtpInput.doOnTextChanged { text, _, _, _ ->
            otpViewModel.enableButton(
                otpCode = text.toString()
            )
        }

        binding.btnValidateOtp.setOnClickListener {
            if (otpViewModel.endTime.value == true || otpViewModel.failedGenerate.value == true) {
                binding.btnValidateOtp.isEnabled = false
                otpViewModel.callGenerateOtp()
                timer.startTimer()
                otpViewModel.endTime.value = false
                otpViewModel.setFailedGenerate(false)
            } else {
                (requireActivity() as LoginActivity).setBaseViewModel(otpViewModel)
                otpViewModel.showLoading.set(true)
                otpViewModel.validateOtp(
                    context = requireContext(),
                    value = binding.etOtpInput.text.toString()
                )
            }
        }

        binding.tvGenerateOtherOtp.setOnClickListener {
            otpViewModel.isThereOtp.value = false
            binding.btnValidateOtp.isVisible = true
            binding.etOtpInput.text?.clear()
            binding.btnValidateOtp.isEnabled = false
            binding.tvOtpGenerate.text = ""
            otpViewModel.callGenerateOtp()
            timer.startTimer()
            otpViewModel.endTime.value = false
            otpViewModel.setFailedGenerate(false)
        }

        binding.etOtpInput.doOnTextChanged { text, _, _, _ ->
            binding.btnValidateOtp.isEnabled = ValidationUtils.validateOtpCode(text.toString())
        }

    }

    private fun observeViewModel() {
        otpViewModel.generateOtpResponse.observe(requireActivity(), Observer { generate ->
            if (isAdded) view?.let {
                when (generate) {
                    is OtpCreationSuccess -> Unit
                    is OtpCreationError -> {
                        if (isAdded) view?.let {
                            val errorMessage: String = when (generate.code) {
                                ERROR_EMPTY_MAIL_PHONE -> getString(R.string.empty_mail_phone_error)
                                ERROR_OTP_EXCEPTION_CODE -> getString(R.string.generic_error_message)
                                else -> generate.message
                            }
                            (requireActivity() as BaseActivity)
                                .showMessage(
                                    getString(R.string.generic_error_title),
                                    errorMessage,
                                    okAction = {})
                        }
                    }
                }
            }
        })

        otpViewModel.validateOtpResponse.observe(requireActivity(), Observer { validate ->
            if (isAdded) view?.let {
                when (validate) {
                    is OtpValidationSuccess -> {
                        isSecondExpiration = 0
                        timer.stopTimer()
                        val bundle: Bundle =
                            bundleOf(AppConstants.VALUE_OTP_NEW_DEVICE to validate.token)
                        findNavController().navigate(R.id.action_otpFragment_to_passwordLoginFragment, bundle)
                        otpViewModel.clearOtpLiveDataValues()
                        binding.etOtpInput.text!!.clear()
                    }
                    is OtpValidationError -> {
                        if (isAdded) view?.let {
                            if (validate.code == AppConstants.SC_STATUS_OTP_IS_NOT_GENERATED) {
                                otpViewModel.otpIsNotGenerated++
                            }
                            val errorMessage: String = when (validate.code) {
                                ERROR_EMPTY_OTP_CODE -> getString(R.string.empty_otp_code)
                                ERROR_OTP_EXCEPTION_CODE -> getString(R.string.otp_does_not_match)
                                else -> validate.message
                            }
                            (requireActivity() as BaseActivity)
                                .showMessage(
                                    getString(R.string.generic_error_title),
                                    errorMessage,
                                    okAction = {
                                        requireActivity().finish()
                                    })
                        }
                    }
                    else -> Unit
                }
            }
        })

        otpViewModel.onEndTime.observe(requireActivity(), Observer { end ->
            if (isAdded && end) view?.let {
                binding.etOtpInput.text?.clear()
                binding.btnValidateOtp.isEnabled = false
                val showConfirm = if (otpViewModel.onIsPhone.value == true)
                    true
                else
                    !end
                binding.btnValidateOtp.isVisible = showConfirm
//                showOtpExpirationMessage()
            }
        })

        otpViewModel.failedGenerate.observe(requireActivity(), Observer { failed ->
            if (isAdded && failed) view?.let {
                binding.etOtpInput.text?.clear()
                binding.btnValidateOtp.isEnabled = false
                timer.stopTimer()
                otpViewModel.endTime.value = true
            }
        })
    }

    private fun showOtpExpirationMessage() {
        when (requireActivity()) {
            is LoginActivity -> {
                if (otpViewModel.isPhone.value == true && otpViewModel.device.value == true)
                    if (otpViewModel.failedGenerate.value == true) {
                        showExpiredMessage()
                        if (isSecondExpiration > 0)
                            isSecondExpiration -= 1
                    } else {
                        isSecondExpiration += 1
                        showExpiredMessagePhone(R.string.otp_user_let_expired_ph, isSecondExpiration == 2)
                    }
                else
                    showExpiredMessage()
            }
            else -> showExpiredMessage()
        }
    }

    private fun showExpiredMessagePhone(m: Int = R.string.otp_user_let_expired, c: Boolean = false) {
        (requireActivity() as BaseActivity)
            .showMessage(
                R.string.generic_error_title,
                m,
                okAction = {
                    if (c)
                        requireActivity().finish()
                }
            )
    }

    private fun showExpiredMessage() {
        Toast
            .makeText(requireActivity(), getString(R.string.otp_expired), Toast.LENGTH_SHORT).show()
    }



}