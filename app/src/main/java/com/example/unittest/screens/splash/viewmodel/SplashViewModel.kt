package com.example.unittest.screens.splash.viewmodel

import android.content.Context
import android.os.Handler
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.example.unittest.R
import com.example.unittest.screens.common.base.BaseViewModel
import com.example.unittest.screens.main.repository.MainRepository
import com.example.unittest.screens.splash.repository.SplashRepository
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.ParamsHelper
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val splashRepository: SplashRepository,
    private val mainRepository: MainRepository
) :
    BaseViewModel() {

    val availableVersion: MutableLiveData<Boolean> = MutableLiveData()
    private var isInit = false
    private var isTimer = false
    val haveToUpdate: ObservableBoolean = ObservableBoolean()


    fun init(context: Context) {
        mainRepository.setUp(context) {
            if (it) {
                splashRepository.getParams(
                    { params ->
                        ParamsHelper.params = params
                        haveToUpdate.set(params.haveToUpdate)
                        //Derive
                        splashRepository.getDerive(
                            context = context,
                            deriveResponse = { deriveResponse ->
                                ParamsHelper.params.derive = deriveResponse.derive
                                mainRepository.getFireBaseToken(
                                    token = { token ->
                                        if (token != null) {
                                            ParamsHelper.fireBaseToken = token
                                            if (isTimer) {
                                                availableVersion.value = params.availableVersion
                                            } else {
                                                isInit = true
                                            }
                                        } else {
                                            showError(
                                                titleId = R.string.generic_error_title,
                                                messageId = R.string.generic_error_message,
                                                okAction = {
                                                    closeActivity.value = true
                                                }
                                            )
                                        }
                                    }
                                )
                            },
                            errorResponse = { error ->
                                showError(
                                    titleId = R.string.generic_error_title,
                                    message = error.errorMessage,
                                    okAction = {
                                        closeActivity.value = true
                                    }
                                )
                            }
                        )
                    },
                    { error ->
                        showError(
                            titleId = R.string.generic_error_title,
                            message = error.errorMessage,
                            okAction = {
                                closeActivity.value = true
                            }
                        )
                    }
                )
            } else {
                showError(
                    titleId = R.string.generic_error_title,
                    messageId = R.string.generic_error_message,
                    okAction = {
                        closeActivity.value = true
                    }
                )
            }
        }
    }

    fun timerSplash() {
        Handler().postDelayed({
            if (isInit) {
                availableVersion.value = ParamsHelper.params.availableVersion
            } else {
                isTimer = true
            }
        }, AppConstants.TIME_SPLASH_SETUP)
    }

}
