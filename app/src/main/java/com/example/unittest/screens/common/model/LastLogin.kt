package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class LastLogin(
    @SerializedName("date")
    val date: String? = "",
    @SerializedName("ip")
    val ip:  String? = ""
)