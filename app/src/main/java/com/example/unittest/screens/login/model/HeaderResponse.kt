package com.example.unittest.screens.login.model

import com.google.gson.annotations.SerializedName

data class HeaderResponse(
    @SerializedName("Date")
    var date: String,
    @SerializedName("Validation")
    var validation: String,
    @SerializedName("Authorization")
    var authorization: String,
    @SerializedName("crossAuthorization")
    var crossAuthorization: String
)