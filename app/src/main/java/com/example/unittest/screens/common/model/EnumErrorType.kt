package com.example.unittest.screens.common.model

enum class EnumErrorType {
    HTTP, CUSTOM, PARSER, USER_LOCKED, WRONG_PASSWORD, NO_NETWORK
}