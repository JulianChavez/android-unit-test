package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class ItemResponse(
    @SerializedName("id") var id : Int = 0,
    @SerializedName("description") var description : String = ""
)