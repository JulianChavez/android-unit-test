package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class LandModel(
    @SerializedName("image")
    val image: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String
)