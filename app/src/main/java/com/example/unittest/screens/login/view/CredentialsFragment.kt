package com.example.unittest.screens.login.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.unittest.R
import com.example.unittest.databinding.FragmentCredentialsBinding
import com.example.unittest.screens.common.model.EnumUserState
import com.example.unittest.screens.common.model.ValidateUserModel
import com.example.unittest.screens.login.viewmodel.LoginViewModel
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.NewDeviceArguments
import com.example.unittest.utils.timeouthelper.DatePickerFragment
import com.google.android.material.textfield.TextInputLayout

class CredentialsFragment : Fragment() {

    private lateinit var binding: FragmentCredentialsBinding
    private val loginViewModel: LoginViewModel by activityViewModels()
    private var birthDate: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_credentials, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = loginViewModel

        listeners()
        observeViewModel()

        return binding.root

    }

    private fun observeViewModel() {
        loginViewModel.userState.observe(viewLifecycleOwner, Observer {
            when(it) {
                EnumUserState.CIVICA_PAY -> {
                    findNavController().navigate(R.id.action_credentialsFragment_to_passwordLoginFragment)
                }
                EnumUserState.DOES_NOT_EXISTS -> {
                    registerUser()
                }
                else -> {
                    Unit
                }
            }
        })

        loginViewModel.userPhone.observe(requireActivity(), Observer { phone ->
            if (isAdded && phone != null) view?.post {
                val newDeviceArguments: NewDeviceArguments = NewDeviceArguments().apply {
                    idType = "01"
                    idData = binding.etDocument.text.toString()
                }
                val bundle =
                    if (loginViewModel.expired) {
                        bundleOf(
                            AppConstants.VALUE_OTP_NEW_DEVICE to newDeviceArguments,
                            AppConstants.KEY_USE_ALLIES to true,
                            AppConstants.VALUE_OTP_REFRESH to phone
                        )
                    } else {
                        bundleOf(
                            AppConstants.VALUE_OTP_PHONE_DEVICE to phone,
                            AppConstants.VALUE_OTP_NEW_DEVICE to newDeviceArguments,
                            AppConstants.KEY_USE_ALLIES to true
                        )
                    }

                findNavController().navigate(R.id.action_credentialsFragment_to_otpFragment, bundle)
                loginViewModel.clearGetPhoneValues()
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun listeners() {
        binding.btnContinue.setOnClickListener {
            validateUser()
        }

        binding.etDate.setOnClickListener {
            showDatePicker()
        }
    }

    private fun validateUser() {
        val validateUserModel = ValidateUserModel(
            requireContext(),
            "01",
            binding.etDocument.text.toString(),
            birthDate
        )

        loginViewModel.validateUser(
            validateUserModel,
            onGetPhone = {
                loginViewModel.getPhoneNumber()
            }
        )

    }

    private fun showDatePicker() {
        val datePicker = DatePickerFragment{ date -> onDateSelected(date) }
        datePicker.show(requireActivity().supportFragmentManager, "datePicker")
    }

    private fun onDateSelected(date: String) {
        var error = false
        if (date == AppConstants.VALUE_YOUNGER) {
            birthDate = ""
            binding.tiDate.setupFormat(show = true, errorMessage = getString(R.string.txt_error_age), color = R.color.colorError)
        } else {
            birthDate = date
            binding.tiDate.setupFormat(show = false, color = R.color.colorAccent)
        }
        binding.etDate.setText(birthDate)
    }

    private fun registerUser() {

    }

    private fun TextInputLayout.setupFormat(show: Boolean, errorMessage: String = "", color: Int = 0) {
        if (show) {
            this.helperText = errorMessage
            this.setHelperTextColor(ContextCompat.getColorStateList(context, color))
            ContextCompat.getColorStateList(context, color)?.let { this.setBoxStrokeColorStateList(it) }
        } else {
            this.isHelperTextEnabled = show
            ContextCompat.getColorStateList(context, color)?.let { this.setBoxStrokeColorStateList(it) }
        }
    }

}