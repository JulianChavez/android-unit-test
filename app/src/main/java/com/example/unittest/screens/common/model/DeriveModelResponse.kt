package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class DeriveModelResponse(
    @SerializedName("derivativeKey")
    val derive: String
)