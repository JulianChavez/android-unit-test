package com.example.unittest.screens.common.model

import com.example.unittest.utils.AppConstants
import com.google.gson.annotations.SerializedName
import java.util.*

data class ParamsModelResponse(
    @SerializedName("availableVersion")
    var availableVersion: Boolean = false,
    @SerializedName("haveToUpdate")
    var haveToUpdate: Boolean = false,
    @SerializedName("lands")
    var lands: List<LandModel> = listOf(),
    @SerializedName("documentType")
    var documentType: List<DocumentTypeModel> = listOf(),
    @SerializedName("retryLoginAllow")
    var retryLoginAllow: Int = 0,
    @SerializedName("phoneNumberLength")
    var phoneNumberLength: Int = 0,
    @SerializedName("maxAllowedDocumentLength")
    var maxAllowedDocumentLength: Int = 0,
    @SerializedName("recoverCodeLength")
    var recoverCodeLength: Int = 6,
    @SerializedName("delayWrongRetry")
    var delayWrongRetry: Int = 0,
    @SerializedName("retryPDF417")
    var retryPDF417: Int = 0,
    @SerializedName("delayBeforeClosePDF417")
    var delayBeforeClosePDF417: Int = 0,
    @SerializedName("expirationTimeCode")
    var expirationTimeCode: Int = 0,
    @SerializedName("putLogs")
    var putLogs: Boolean = false,
    @SerializedName("showLands")
    var showLands: Boolean = false,
    @SerializedName("minimumAgeAllowed")
    var minimumAgeAllowed: Int = 0,
    @SerializedName("termsAndConditions")
    var termsAndConditions: List<TermsAndConditionsModel> = listOf(),
    @SerializedName("retryEmailAllowed")
    var retryEmailAllowed: Int = 0,
    @SerializedName("phoneNumberPrefix")
    var phoneNumberPrefix: List<String> = listOf(),
    @SerializedName("daysMovementsAvailable")
    var daysMovementsAvailable: Long = 0L,
    @SerializedName("waitingTimeConfirmTransaction")
    var waitingTimeConfirmTransaction: Long = 60000L,
    @SerializedName("derive")
    var derive: String = "",
    @SerializedName("cellPhoneRechageMinValue")
    var cellPhoneRechageMinValue: Int = 0,
    @SerializedName("cellPhoneRechangeMaxValue")
    var cellPhoneRechangeMaxValue: Int = 0,
    @SerializedName("miFareRequired")
    var miFareRequired: Boolean = false,
    @SerializedName("qrCodeDueTimeInSeconds")
    var qrCodeDueTimeInSeconds: Int = 4,
    @SerializedName("qrIterationsLife")
    var qrIterationsLife: Short = 15,
    @SerializedName("qrExpirationEnable")
    var qrExpirationEnable: Boolean = false,
    @SerializedName("withdrawMinValue")
    var withdrawMinValue: Int = 0,
    @SerializedName("withdrawMaxValue")
    var withdrawMaxValue: Int = 0,
    @SerializedName("sendMoneyMinValue")
    var sendMoneyMinValue: Int = 0,
    @SerializedName("sendMoneyMaxValue")
    var sendMoneyMaxValue: Int = 0,
    @SerializedName("paymentsMinValue")
    var paymentsMinValue: Int = 0,
    @SerializedName("paymentsMaxValue")
    var paymentsMaxValue: Int = 0,
    @SerializedName("purchaseQrMaxValue")
    var purchaseQrMaxValue: Int = 0,
    @SerializedName("purchaseQrMinValue")
    var purchaseQrMinValue: Int = 0,
    @SerializedName("whatHappened")
    var whatHappened: List<ItemResponse> = listOf(),
    @SerializedName("professions")
    var professions: List<ItemResponse> = listOf(),
    @SerializedName("transportProfiles")
    var transportProfiles: List<ItemResponse> = listOf(),
    @SerializedName("transportProfilesTypes")
    var transportProfilesTypes: List<ItemResponse> = listOf(),
    @SerializedName("minAgeToElder")
    var minAgeToElder: Int = 60,
    @SerializedName("generalParameters")
    var generalParameters: GeneralParameters = GeneralParameters()
) {
    fun getDocumentTypeByAbbreviation(abbreviation: String): DocumentTypeModel {
        val document = documentType.firstOrNull{ it.abbreviation == abbreviation }
        return document ?: documentType.first()
    }
    fun getDocumentTypeById(idType: String): DocumentTypeModel {
        val document = documentType.firstOrNull{ it.idType == idType }
        return document ?: documentType.first()
    }
    fun getFrequentQrProfile(): ItemResponse {
        val item = transportProfilesTypes.firstOrNull {
            it.description.toLowerCase(Locale.ROOT) == AppConstants.VALUE_FREQUENT
                    || it.id == AppConstants.VALUE_FREQUENT_CODE
        }
        return item ?: transportProfilesTypes.first()
    }
    fun getTypeProfile(id:Int = AppConstants.VALUE_FREQUENT_CODE, description:String = AppConstants.VALUE_FREQUENT):ItemResponse{
        val item = transportProfilesTypes.firstOrNull {
            it.description.toLowerCase(Locale.ROOT) == description
                    || it.id == id
        }
        return item ?: transportProfilesTypes.firstOrNull() ?: ItemResponse()
    }
}