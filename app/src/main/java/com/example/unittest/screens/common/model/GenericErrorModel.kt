package com.example.unittest.screens.common.model

data class GenericErrorModel(
    var title: String? = null,
    var titleId: Int? = null,
    var message: String? = null,
    var messageId: Int? = null,
    var btnOk: String? = null,
    var btnCancel: String? = null,
    var okAction: (() -> Unit)? = null,
    var cancelAction: (() -> Unit)? = null
)