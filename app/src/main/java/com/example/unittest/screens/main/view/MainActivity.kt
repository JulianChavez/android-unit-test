package com.example.unittest.screens.main.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.unittest.R
import com.example.unittest.databinding.ActivityMainBinding
import com.example.unittest.screens.common.base.BaseActivity
import com.example.unittest.screens.main.viewmodel.MainViewModel

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        val viewModel: MainViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        //binding.viewModel = viewModel

    }
}
