package com.example.unittest.screens.common.base

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.example.unittest.R
import com.example.unittest.utils.timeouthelper.TimeOutHelper
import com.example.unittest.utils.timeouthelper.TimeOutListener
import com.valid.utils.VsshLogger
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject

open class BaseActivity : DaggerAppCompatActivity(), TimeOutListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var baseViewModel: BaseViewModel
    lateinit var builder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        builder = AlertDialog.Builder(this)

    }

    private companion object {
        val TAG = BaseActivity::class.java.simpleName
    }

    private var timerTask: TimerTask? = null

    fun setBaseViewModel(baseViewModel: BaseViewModel) {
        this.baseViewModel = baseViewModel
        this.observeBaseVariables()
    }

    private fun observeBaseVariables() {
        //TODO: Define Base variables
    }

    fun showMessage(title: Int, message: Int, okAction: (() -> Unit)? = null, cancelAction: (() -> Unit)? = null, @StringRes okText: Int = R.string.dialog_ok_button, @StringRes cancelText: Int = R.string.dialog_cancel_button) {
        showMessage(
            getString(title),
            getString(message),
            okAction,
            cancelAction,
            getString(okText),
            getString(cancelText)
        )
    }

    fun showMessage(title: String, message: String, okAction: (() -> Unit)? = null, cancelAction: (() -> Unit)? = null, okText: String = getString(R.string.dialog_ok_button), cancelText: String = getString(R.string.dialog_cancel_button)) {
        builder.setMessage(message)
        builder.setTitle(title)
        builder.setPositiveButton(okText) { dialog, _ ->
            okAction?.let {
                okAction.invoke()
            }
            dialog.dismiss()
        }
        cancelAction?.let {
            builder.setNegativeButton(cancelText) { dialog, _ ->
                cancelAction.invoke()
                dialog.dismiss()
            }
        }

        builder.setCancelable(false)
        builder.show()
    }

    override fun onUserInteraction() {
        this.startUserSession()
        super.onUserInteraction()
    }

    private fun startUserSession() {
        if (TimeOutHelper.activeSession) {
            timerTask?.let {
                timerTask?.cancel()
            }

            if (TimeOutHelper.timer != null) {
                TimeOutHelper.timer?.cancel()
                TimeOutHelper.timer?.purge()
            }

            TimeOutHelper.timeOut?.let {
                timerTask = object : TimerTask() {
                    override fun run() {
                        onLogoutSession()
                    }
                }
                TimeOutHelper.timer = Timer()
                TimeOutHelper.timer?.schedule(timerTask, it)
            }
        }
    }

    override fun onLogoutSession() {
        VsshLogger.logDebug(TAG, "LOGOUT")
        TimeOutHelper.activeSession = false
        runOnUiThread { this.callTimeOutMain() }
    }

    private fun callTimeOutMain() {
        //TODO: show custom alert
    }
}
