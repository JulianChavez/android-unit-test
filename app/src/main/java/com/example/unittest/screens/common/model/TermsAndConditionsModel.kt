package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TermsAndConditionsModel(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("content")
    val content: String,
    @SerializedName("order")
    val order: Int,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("nested")
    val nested: Boolean,
    @SerializedName("accepted")
    var accepted: Boolean
):Serializable