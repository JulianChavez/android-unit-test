package com.example.unittest.screens.splash.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.unittest.BuildConfig
import com.example.unittest.screens.common.base.BaseRepository
import com.example.unittest.screens.common.model.*
import com.example.unittest.utils.AppConstants
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.valid.communication.helpers.CommunicationConstants
import com.valid.security.helpers.KeyHelper
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.model.SetupModel
import com.valid.vssh_android_core.utils.VsshCoreConstants
import javax.inject.Inject
import javax.inject.Singleton

class SplashRepository @Inject constructor(private val coreManager: VsshCoreManager) :
    BaseRepository() {

    lateinit var isSetup: (value: Boolean) -> Unit

    fun setUp(context: Context?, isSetup: (value: Boolean) -> Unit) {
        this.isSetup = isSetup

        val setupModel = SetupModel()
        setupModel.context = context
        setupModel.urlBase = BuildConfig.BASE_URL
        setupModel.apiGetCertificates = AppConstants.API_GET_CERTIFICATES
        setupModel.apiExchange = AppConstants.API_EXCHANGE_KEY
        setupModel.publicKey = BuildConfig.PUBLIC_KEY
        coreManager.setup(
            setupModel,
            {
                this.isSetup(it)
            },
            {
                this.isSetup(false)
            }
        )
    }

    fun getParams(
        paramsResponse: (value: ParamsModelResponse) -> Unit,
        errorResponse: (value: ErrorModelResponse) -> Unit
    ) {
        communicationManager = coreManager.communicationManager

        val headers = mapOf(
            AppConstants.KEY_CUSTOMER to AppConstants.VALUE_CUSTOMER_GET_PARAMS
        )

        val principalData = mapOf<String, Any>(
            AppConstants.KEY_APPLICATION_CODE to AppConstants.VALUE_APPLICATION_CODE
        )

        val additionalData = mapOf<String, Any>(
            AppConstants.KEY_SO to AppConstants.VALUE_SO,
            AppConstants.KEY_VERSION to BuildConfig.VERSION_NAME,
            AppConstants.KEY_LANGUAGE to AppConstants.VALUE_LANGUAGE
        )

        val body = mapOf<String, Any>(
            VsshCoreConstants.PARAM_PRINCIPAL_DATA to JsonParser().parse(Gson().toJson(principalData)),
            VsshCoreConstants.PARAM_ADDITIONAL_DATA to JsonParser().parse(Gson().toJson(additionalData))
        )

        val request = VsshCommunicationRequest(
            MethodType.POST,
            AppConstants.API_GET_PARAMS + AppConstants.EP_PARAMS,
            headers,
            body,
            sslPinning = true,
            responseEncrypted = true,
            secureRequest = true,
            addSessionIdToRequest = true
        )

        consumeVsshService(
            request,
            ParamsModelResponse::class.java,
            {
                if (it.statusCode == CommunicationConstants.STATUS_CODE_SUCCESS) {
                    paramsResponse(it.response as ParamsModelResponse)
                } else {
                    errorResponse(
                        ErrorModelResponse(
                            EnumErrorType.CUSTOM,
                            it.message
                        )
                    )
                }
            },
            {
                errorResponse(
                    ErrorModelResponse(
                        EnumErrorType.HTTP,
                        it.errorData.message
                    )
                )
            }
        )
    }

    fun getDerive(
        context: Context,
        deriveResponse: (value: DeriveModelResponse) -> Unit,
        errorResponse: (value: ErrorModelResponse) -> Unit
    ) {
        communicationManager = coreManager.communicationManager

        val body = mapOf<String, Any>(
            AppConstants.KEY_AES_KEY to AppConstants.VALUE_AES_KEY,
            AppConstants.KEY_APPLICATION_CODE to AppConstants.VALUE_APPLICATION_CODE,
            AppConstants.KEY_DERIVATION_DATA to KeyHelper.getFingerPrint(context)
        )

        val request = VsshCommunicationRequest(
            MethodType.POST,
            AppConstants.API_DERIVE + AppConstants.EP_DERIVE,
            null,
            body,
            sslPinning = true,
            responseEncrypted = true,
            secureRequest = true,
            addSessionIdToRequest = true
        )

        consumeVsshService(
            request,
            DeriveModelResponse::class.java,
            {
                if (it.statusCode == CommunicationConstants.STATUS_CODE_SUCCESS) {
                    deriveResponse(it.response as DeriveModelResponse)
                } else {
                    errorResponse(
                        ErrorModelResponse(
                            EnumErrorType.CUSTOM,
                            it.message
                        )
                    )
                }
            },
            {
                errorResponse(
                    ErrorModelResponse(
                        EnumErrorType.HTTP,
                        it.errorData.message
                    )
                )
            }
        )
    }
}
