package com.example.unittest.screens.login.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.unittest.R
import com.example.unittest.databinding.FragmentPasswordLoginBinding
import com.example.unittest.screens.login.viewmodel.LoginViewModel
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.NewDeviceArguments


class PasswordLoginFragment : Fragment() {

    private lateinit var binding: FragmentPasswordLoginBinding
    private val viewModel: LoginViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_password_login, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        setupListeners()
        observeViewModel()
        return binding.root
    }

    private fun setupListeners() {
        binding.btnContinue.setOnClickListener {
            val alliesData: String? = arguments?.getString(AppConstants.VALUE_OTP_NEW_DEVICE)
            val expiredData: String? = arguments?.getString(AppConstants.VALUE_OTP_EXPIRED)
            viewModel.login(
                requireActivity(),
                binding.etPassword.text.toString(),
                alliesData,expiredData
            )
        }
    }

    private fun observeViewModel() {
        viewModel.loginState.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_passwordLoginFragment_to_homeFragment)
            }
        })
    }
}