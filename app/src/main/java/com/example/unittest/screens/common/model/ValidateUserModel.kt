package com.example.unittest.screens.common.model

import android.content.Context
import com.google.gson.annotations.SerializedName

data class ValidateUserModel (
    @SerializedName("context")
    var context: Context,
    @SerializedName("idType")
    var idType: String,
    @SerializedName("idData")
    var idData: String,
    @SerializedName("birthDate")
    var birthDate: String
)
