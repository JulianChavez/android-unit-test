package com.example.unittest.screens.login.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.unittest.R
import com.example.unittest.databinding.ActivityLoginBinding
import com.example.unittest.screens.common.base.BaseActivity
import com.example.unittest.screens.login.viewmodel.LoginViewModel
import com.example.unittest.screens.otp.viewmodel.OtpViewModel

class LoginActivity : BaseActivity() {
    lateinit var viewModel: LoginViewModel
    lateinit var otpViewModel: OtpViewModel
    private lateinit var binding: ActivityLoginBinding

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.lifecycleOwner = this

        viewModel = ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]
        otpViewModel = ViewModelProviders.of(this, viewModelFactory)[OtpViewModel::class.java]
        binding.viewModel = viewModel
        binding.baseViewModel = viewModel
        setBaseViewModel(viewModel)

    }
}