package com.example.unittest.screens.login.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.unittest.R
import com.example.unittest.screens.common.base.BaseViewModel
import com.example.unittest.screens.common.model.*
import com.example.unittest.screens.login.model.HeaderResponse
import com.example.unittest.screens.login.repository.LoginRepository
import com.example.unittest.screens.main.repository.MainRepository
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.ParamsHelper
import com.example.unittest.utils.PreferencesHelper
import com.example.unittest.utils.ValidationUtils
import com.google.gson.Gson
import com.valid.communication.helpers.CommunicationConstants
import com.valid.vssh_android_core.model.response.ClientHelloModelResponse
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: LoginRepository,
    private val mainRepository: MainRepository
) : BaseViewModel() {

    lateinit var validateUserModel: ValidateUserModel

    var userState: MutableLiveData<EnumUserState> = MutableLiveData()
    var dataUser = MutableLiveData<UserData>()
    var userName = MutableLiveData<String>("")

    var clientHelloError = MutableLiveData<ErrorModelResponse>()
    var responseClientHello = MutableLiveData<ClientHelloModelResponse>()

    private val _userExists = MutableLiveData<EnumUserState>()
    val userExists: LiveData<EnumUserState> = _userExists

    private val _userPhone = MutableLiveData<String>()
    val userPhone: LiveData<String> = _userPhone

    private val _errorUserPhone = MutableLiveData<ErrorModelResponse>()
    val errorUserPhone: LiveData<ErrorModelResponse> = _errorUserPhone


    private val _loginError = MutableLiveData(false)
    val loginError: MutableLiveData<Boolean> = _loginError

    var loginState: MutableLiveData<Boolean> = MutableLiveData()


    var isDeviceAssigned: Boolean = true
    var expired: Boolean = false


    fun validateUser(
        validateUserModel: ValidateUserModel,
        onGetPhone: (() -> Unit)? = null,
        validate: Boolean = false
    ) {
        this.validateUserModel = validateUserModel
        mainRepository.setUp(validateUserModel.context) {
            if (it) {
                mainRepository.clientHello(
                    validateUserModel,
                    { clientHelloResponse ->
                        showLoading.set(false)
                        responseClientHello.value = clientHelloResponse
                        val userStateResponse =
                            EnumUserState.values()[clientHelloResponse.userState]


                        isDeviceAssigned = clientHelloResponse.deviceAssigned

                        expired = userStateResponse == EnumUserState.EXPIRED

                        if ((!isDeviceAssigned && userStateResponse == EnumUserState.CIVICA_PAY) || expired)
                            onGetPhone?.invoke()
                        else {
                            if(!validate) {
                                userState.value = userStateResponse
                            }else {
                                _userExists.value=userStateResponse
                            }
                        }

                    },
                    { error ->
                        showLoading.set(false)
                        clientHelloError.value = error
                        showError(
                            titleId = R.string.generic_error_title,
                            message = error.errorMessage,
                            okAction = {
                                closeActivity.value = true
                            }
                        )
                    }
                )
            } else {
                showLoading.set(false)
                showError(
                    titleId = R.string.generic_error_title,
                    messageId = R.string.generic_error_message,
                    okAction = {
                        closeActivity.value = true
                    }
                )
            }
        }
    }

    fun login(
        context: Context,
        password: String,
        newDevice: String? = null,
        expired: String? = null
    ) {
        showLoading.set(true)
        repository.login(
            password,
            { loginResponse ->
                showLoading.set(false)

                when (loginResponse.statusCode) {
                    CommunicationConstants.STATUS_CODE_SUCCESS -> {
                        PreferencesHelper(context).stringKeyValidation =
                            Gson().fromJson(loginResponse.headers, HeaderResponse::class.java)
                                .validation
                        loginState.value = true

                        ParamsHelper.mapToken =
                            Gson().fromJson(loginResponse.headers, HeaderResponse::class.java)
                                .crossAuthorization

                        PreferencesHelper(context).lastLoginAttempt = 0
//                        setEnablePass(true)
                    }

                    else -> {
                        _loginError.value = true

                        showError(
                            titleId = R.string.generic_error_title,
                            message = loginResponse.message,
                            okAction = {
                                closeActivity.value = true
                            }
                        )
                    }
                }
            },
            { error ->
                showLoading.set(false)
                if (error.internalErrorCode == AppConstants.SC_EXPIRED_SESSION || error.internalErrorCode == AppConstants.SC_EXPIRED_SESSION_CORE) {
//                    errorSessionExpired.value = true
                } else {
                    when (error.errorType) {
                        EnumErrorType.PARSER, EnumErrorType.CUSTOM, EnumErrorType.HTTP -> {
                            showError(
                                titleId = R.string.generic_error_title,
                                message = error.errorMessage,
                                okAction = {
                                    closeActivity.value = true
                                }
                            )
                        }

                        EnumErrorType.USER_LOCKED -> {
                            if (PreferencesHelper(context).lastLoginAttempt <= 0L)
                                PreferencesHelper(context).lastLoginAttempt =
                                    System.currentTimeMillis()
//                            setEnablePass(false)
                        }

                        EnumErrorType.WRONG_PASSWORD -> {
                            _loginError.value = true
                        }
                        else -> Unit
                    }
                }
            },
            newDevice,
            expired
        )
    }

    fun getPhoneNumber() {
        showLoading.set(true)
        repository.getUserData(
            onSuccess = { data ->
                if (!ValidationUtils.isNullEmptyOrBlankText(data.cellPhone)) {
                    _userPhone.value = data.cellPhone
                    dataUser.value = data
                    userName.value = data.name + " ${data.lastName}"
                }
                else
                    _errorUserPhone.value = ErrorModelResponse(errorMessage = null)
                showLoading.set(false)
            },
            onError = { error ->
                if (error.internalErrorCode == AppConstants.SC_EXPIRED_SESSION || error.internalErrorCode == AppConstants.SC_EXPIRED_SESSION_CORE) {
//                    errorSessionExpired.value = true
                } else {
                    _errorUserPhone.value = error
                }
                showLoading.set(false)
            }
        )
    }


    fun clearGetPhoneValues() {
        _userPhone.value = null
        _errorUserPhone.value = null
    }

} 