package com.example.unittest.screens.common.model

data class VsshCommunicationRequest(
    var method: MethodType = MethodType.POST,

    var path: String,

    var headers: Map<String, String>?,

    var body: Map<String, Any>?,

    var sslPinning: Boolean = true,

    var responseEncrypted: Boolean = true,

    var secureRequest: Boolean = true,

    var addSessionIdToRequest: Boolean = true
)