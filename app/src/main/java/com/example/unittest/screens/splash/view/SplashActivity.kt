package com.example.unittest.screens.splash.view

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.unittest.R
import com.example.unittest.databinding.ActivitySplashBinding
import com.example.unittest.screens.common.base.BaseActivity
import com.example.unittest.screens.login.view.LoginActivity
import com.example.unittest.screens.main.view.MainActivity
import com.example.unittest.screens.splash.viewmodel.SplashViewModel

public class SplashActivity : BaseActivity() {

    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val binding: ActivitySplashBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_splash)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)

        viewModel.timerSplash()
        viewModel.init(this)

        observeViewModel()


    }

    private fun observeViewModel() {
        startActivity(Intent(this, LoginActivity::class.java))

    }
}
