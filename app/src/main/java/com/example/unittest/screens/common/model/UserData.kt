package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("address")
    var address: String = "",
    @SerializedName("cellphone")
    val cellPhone: String = "",
    @SerializedName("ipAddress")
    val ipAddress: String = "",
    @SerializedName("lastName")
    val lastName: String = "",
    @SerializedName("miFareLocked")
    val miFareLocked: Boolean = false,
    @SerializedName("miFareProfile")
    val miFareProfile: MiFareProfile = MiFareProfile(),
    @SerializedName("name")
    val name: String = "",
    @SerializedName("profesion")
    var profesion: String = "",
    @SerializedName("qrProfile")
    val qrProfile: QrProfile = QrProfile(),
    @SerializedName("email")
    var email: String? = "",
    @SerializedName("lastLogin")
    var lastLogin: LastLogin? = LastLogin(),
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("company")
    var company: String = "",
    @SerializedName("profesionId")
    var profesionId: Int = 0
)