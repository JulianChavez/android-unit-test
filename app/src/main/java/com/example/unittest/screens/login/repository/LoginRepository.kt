package com.example.unittest.screens.login.repository

import android.content.Context
import com.example.unittest.BuildConfig
import com.example.unittest.utils.PreferencesHelper
import com.example.unittest.screens.common.base.BaseRepository
import com.example.unittest.screens.common.model.*
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.ParamsHelper
import com.example.unittest.utils.ValidationUtils
import com.valid.communication.helpers.CommunicationConstants
import com.valid.communication.managers.communicationmanager.CommunicationManagerImp
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.exception.CommunicationException
import com.valid.vssh_android_core.model.ClientHelloModel
import com.valid.vssh_android_core.model.LoginModel
import com.valid.vssh_android_core.model.SetupModel
import com.valid.vssh_android_core.model.response.ClientHelloModelResponse
import com.valid.vssh_android_core.model.response.CoreErrorModelResponse
import com.valid.vssh_android_core.model.response.CoreGenericModelResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginRepository @Inject constructor(private val coreManager: VsshCoreManager) :
    BaseRepository() {


    fun login(
        passwordUser: String,
        successLogin: (value: CoreGenericModelResponse) -> Unit,
        errorResponse: (value: ErrorModelResponse) -> Unit,
        newDevice: String? = null,
        expired: String? = null
    ) {

        val customerValue =
            when {
                newDevice == null && expired == null -> AppConstants.VALUE_CUSTOMER_LOGIN
                newDevice != null -> AppConstants.VALUE_CUSTOMER_NEW_DEVICE
                expired != null -> AppConstants.VALUE_CUSTOMER_EXPIRED
                else -> ""
            }

        val mapHeaders =
            hashMapOf(AppConstants.KEY_CUSTOMER to customerValue)

        val loginModel = LoginModel().apply {
            password = passwordUser
            api = AppConstants.API_LOGIN
            publicKey = BuildConfig.PUBLIC_KEY
            headers = mapHeaders
            if (!ValidationUtils.isNullEmptyOrBlankText(newDevice)  || !ValidationUtils.isNullEmptyOrBlankText(expired)) {
                val data=newDevice?:expired?:""
                val dataAdditional =
                    mapOf(AppConstants.KEY_AUTHORIZATION_CODE to data)
                additionalData = dataAdditional
            }


        }

        try {
            coreManager.login(
                loginModel,
                { response: CoreGenericModelResponse ->
                    when (response.statusCode) {
                        CommunicationConstants.STATUS_CODE_SUCCESS ->
                            successLogin(response)

                        AppConstants.STATUS_CODE_WRONG_PASSWORD ->
                            errorResponse(
                                ErrorModelResponse(
                                    EnumErrorType.WRONG_PASSWORD,
                                    response.message
                                )
                            )

                        AppConstants.SC_LOGIN_LOCKED_USER ->
                            errorResponse(
                                ErrorModelResponse(
                                    EnumErrorType.USER_LOCKED,
                                    response.message
                                )
                            )
                        else -> {
                            errorResponse(
                                ErrorModelResponse(
                                    EnumErrorType.CUSTOM,
                                    response.message
                                )
                            )
                        }
                    }
                }
            ) { error: CoreErrorModelResponse ->
                errorResponse(
                    ErrorModelResponse(
                        EnumErrorType.HTTP,
                        error.errorMessage,
                        error.internalErrorCode
                    )
                )
            }
        } catch (e: CommunicationException) {
            errorResponse(
                ErrorModelResponse(
                    EnumErrorType.CUSTOM,
                    e.message
                )
            )
        }
    }

    fun getUserData(
        onSuccess: (data: UserData) -> Unit,
        onError: (error: ErrorModelResponse) -> Unit
    ) {
        communicationManager = coreManager.communicationManager

        val headers =
            hashMapOf(AppConstants.KEY_ACCEPT_ENCODING to AppConstants.VALUE_DEFLATE)

        val request = VsshCommunicationRequest(
            MethodType.POST,
            AppConstants.API_GET_USER_DATA,
            headers,
            null,
            sslPinning = true,
            responseEncrypted = true,
            secureRequest = true,
            addSessionIdToRequest = true
        )

        consumeVsshService(
            request = request,
            responseType = UserData::class.java,
            response = { response ->
                if (response.statusCode == CommunicationConstants.STATUS_CODE_SUCCESS)
                    onSuccess(response.response as UserData)
                else
                    onError(ErrorModelResponse(EnumErrorType.CUSTOM, response.message, response.errCode))
            },
            error = { error ->
                onError(ErrorModelResponse(EnumErrorType.HTTP, error.errorData.message, error.httpStatusCode))
            }
        )
    }

}