package com.example.unittest.screens.otp.repository

import android.util.Log
import com.example.unittest.screens.otp.view.OtpFragment
import com.davivienda.daviplataforthirdparties.manager.DaviplataSDKManager
import com.davivienda.daviplataforthirdparties.manager.DaviplataSDKManagerImp
import com.davivienda.daviplataforthirdparties.models.ConfigurationEntity
import com.example.unittest.BuildConfig
import com.example.unittest.screens.common.base.BaseRepository
import com.example.unittest.screens.login.model.ValidateOtpRequestModel
import com.example.unittest.screens.otp.model.OtpCreationError
import com.example.unittest.screens.otp.model.OtpCreationSuccess
import com.example.unittest.screens.otp.model.OtpValidationError
import com.example.unittest.screens.otp.model.OtpValidationSuccess
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.AppConstants.Companion.API_OTP_GENERATE
import com.example.unittest.utils.AppConstants.Companion.API_OTP_VALIDATE
import com.example.unittest.utils.AppConstants.Companion.ERROR_EMPTY_MAIL_PHONE
import com.example.unittest.utils.AppConstants.Companion.ERROR_EMPTY_OTP_CODE
import com.example.unittest.utils.AppConstants.Companion.ERROR_OTP_EXCEPTION_CODE
import com.example.unittest.utils.AppConstants.Companion.KEY_APPLICATION_CODE
import com.example.unittest.utils.AppConstants.Companion.KEY_CUSTOMER
import com.example.unittest.utils.AppConstants.Companion.KEY_EMAIL
import com.example.unittest.utils.AppConstants.Companion.KEY_PHONE
import com.example.unittest.utils.AppConstants.Companion.KEY_TYPE
import com.example.unittest.utils.AppConstants.Companion.VALUE_APPLICATION_CODE
import com.example.unittest.utils.AppConstants.Companion.VALUE_GENERATE_OTP
import com.example.unittest.utils.AppConstants.Companion.VALUE_VALIDATE_OTP
import com.example.unittest.utils.ValidationUtils
import com.valid.communication.helpers.CommunicationConstants
import com.valid.utils.VsshLogger
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.exception.CommunicationException
import com.valid.vssh_android_core.model.GenerateOtpModel
import com.valid.vssh_android_core.model.ValidateOtpModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OtpRepository @Inject constructor(private val coreManager: VsshCoreManager) :
    BaseRepository() {

    fun generateOtp(
        phone: String = "",
        mail: String = "",
        onSuccess: (data: OtpCreationSuccess, otp: String) -> Unit,
        onError: (value: OtpCreationError) -> Unit
    ) {

        val dataAdditional = mapOf<String, Any>(KEY_APPLICATION_CODE to VALUE_APPLICATION_CODE)
        val headersService = mapOf(KEY_CUSTOMER to VALUE_GENERATE_OTP)

        val generateOtpModel = GenerateOtpModel().apply {
            when {
                phone.isNotEmpty() && phone.isNotBlank() -> cellphone = phone
                mail.isNotEmpty() && mail.isNotBlank() -> email = mail
                else -> onError(OtpCreationError(code = ERROR_EMPTY_MAIL_PHONE))
            }
            api = API_OTP_GENERATE
            additionalData = dataAdditional
            headers = headersService

        }

        try {
            coreManager.generateOtp(
                generateOtpModel,
                { response ->
                    if (response.statusCode == CommunicationConstants.STATUS_CODE_SUCCESS) {
                        Log.d("OTP_CODE", response.message)
                        onSuccess(OtpCreationSuccess, response.message)
                    } else {
                        onError(OtpCreationError(response.message, response.statusCode))
                    }
                },
                { error ->
                    onError(
                        OtpCreationError(
                            error.errorMessage,
                            error.internalErrorCode,
                            error.httpErrorCode
                        )
                    )
                })
        } catch (e: CommunicationException) {
            VsshLogger.logError(OtpFragment::class.simpleName, e.message, e)
            onError(OtpCreationError(e.message ?: "", code = ERROR_OTP_EXCEPTION_CODE))
        }
    }

    fun validateOtp(
        request: ValidateOtpRequestModel,
        onSuccess: (data: OtpValidationSuccess) -> Unit,
        onError: (value: OtpValidationError) -> Unit
    ) {

        validateOtp(
            request.otpValue,
            request.isPhone,
            onSuccess = { response ->
                if (request.isPhone && request.allies)
                    authorize(
                        request,
                        onSuccess,
                        onError
                    )
                else
                    onSuccess(response)
            },
            onError = onError
        )
    }

    private fun validateOtp(
        otpCode: String,
        isPhone: Boolean = false,
        onSuccess: (data: OtpValidationSuccess) -> Unit,
        onError: (value: OtpValidationError) -> Unit
    ) {

        val otpType = if (isPhone) KEY_PHONE else KEY_EMAIL
        val dataAdditional = mapOf<String, Any>(
            KEY_APPLICATION_CODE to VALUE_APPLICATION_CODE,
            KEY_TYPE to otpType
        )
        val headersService = mapOf(KEY_CUSTOMER to VALUE_VALIDATE_OTP)

        val validateOtpModel = ValidateOtpModel().apply {
            when {
                otpCode.isNotEmpty() && otpCode.isNotBlank() -> otp = otpCode
                else -> onError(OtpValidationError(code = ERROR_EMPTY_OTP_CODE))
            }
            api = API_OTP_VALIDATE
            headers = headersService
            additionalData = dataAdditional
        }

        try {
            coreManager.validateOtp(
                validateOtpModel,
                { success ->
                    if (success.statusCode == CommunicationConstants.STATUS_CODE_SUCCESS)
                        onSuccess(OtpValidationSuccess())
                    else
                        onError(OtpValidationError(success.message, success.statusCode))
                },
                { error ->
                    onError(
                        OtpValidationError(
                            error.errorMessage,
                            error.internalErrorCode,
                            error.httpErrorCode
                        )
                    )
                })
        } catch (e: CommunicationException) {
            VsshLogger.logError(OtpFragment::class.simpleName, e.message, e)
            onError(OtpValidationError(message = e.message ?: "", code = ERROR_OTP_EXCEPTION_CODE))
        }
    }

    private fun authorize(
        request: ValidateOtpRequestModel,
        onSuccess: (data: OtpValidationSuccess) -> Unit,
        onError: (value: OtpValidationError) -> Unit
    ) {
        val configurationEntity = ConfigurationEntity().apply {
            applicationCode = BuildConfig.ALIADOS_APPLICATION_CODE
            baseUrl = BuildConfig.ALIADOS_BASE_URL
            publicKey = BuildConfig.ALIADOS_PUBLIC_KEY
            exchangeKey = AppConstants.API_ALIADOS_EXCHANGE
            authorizeService = AppConstants.API_ALIADOS_AUTHORIZE
        }

        val daviplataSDK : DaviplataSDKManager = DaviplataSDKManagerImp()

        daviplataSDK.setup(
            request.context,
            configurationEntity
        )

        daviplataSDK.authorize(
            BuildConfig.ALIADOS_APPLICATION_CODE,
            request.idType,
            request.idData,
            request.otpValue
        ) { token, error ->
            if (!ValidationUtils.isNullEmptyOrBlankText(error)) {
                onError(OtpValidationError(message = error))
            } else {
                onSuccess(OtpValidationSuccess(token = token ?: ""))
            }
        }
    }
}