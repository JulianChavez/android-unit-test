package com.example.unittest.screens.common.model

enum class EnumUserState {
    CIVICA_PAY, SEMI_PRIVATE, OLD_APP, ONLY_MY_FARE, DOES_NOT_EXISTS, LOCKED, EXPIRED
}