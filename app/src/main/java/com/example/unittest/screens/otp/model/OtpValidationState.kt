package com.example.unittest.screens.otp.model

sealed class OtpValidationState
data class OtpValidationSuccess(val token: String = "") : OtpValidationState()
data class OtpValidationError(val message: String = "", val code: Int = 0, val httpCode: Int = 0) :
    OtpValidationState()