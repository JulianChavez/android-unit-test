package com.example.unittest.screens.login.model

import android.content.Context

data class ValidateOtpRequestModel(
    val context: Context?,
    val otpValue: String,
    val idType: String? = null,
    val idData: String? = null,
    val isPhone: Boolean,
    var allies: Boolean = false
)