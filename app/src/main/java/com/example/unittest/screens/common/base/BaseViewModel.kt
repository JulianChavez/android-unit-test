package com.example.unittest.screens.common.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.unittest.screens.common.model.GenericErrorModel

open class BaseViewModel : ViewModel() {

    val genericError = MutableLiveData<GenericErrorModel>()
    val closeActivity = MutableLiveData<Boolean>()
    val errorSessionExpired = MutableLiveData<Boolean>()
    val showLoading = ObservableBoolean()




    protected open fun showError(
        titleId: Int? = null,
        message: String? = null,
        btnOk: String? = null,
        btnCancel: String? = null,
        okAction: (() -> Unit)? = null,
        cancelAction: (() -> Unit)? = null
    ) {
        val genericErrorModel = GenericErrorModel()
        genericErrorModel.titleId = titleId
        genericErrorModel.message = message
        genericErrorModel.btnOk = btnOk
        genericErrorModel.btnCancel = btnCancel
        genericErrorModel.okAction = okAction
        genericErrorModel.cancelAction = cancelAction
        genericError.value = genericErrorModel
        showLoading.set(false)
    }

    protected open fun showError(
        titleId: Int? = null,
        messageId: Int? = null,
        btnOk: String? = null,
        btnCancel: String? = null,
        okAction: (() -> Unit)? = null,
        cancelAction: (() -> Unit)? = null
    ) {
        val genericErrorModel = GenericErrorModel()
        genericErrorModel.titleId = titleId
        genericErrorModel.messageId = messageId
        genericErrorModel.btnOk = btnOk
        genericErrorModel.btnCancel = btnCancel
        genericErrorModel.okAction = okAction
        genericErrorModel.cancelAction = cancelAction
        genericError.value = genericErrorModel
        showLoading.set(false)
    }
}
