package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class GeneralParameters(

    @SerializedName("Prefijos Movil") val prefijosMovil : String = "",
    @SerializedName("PAR_006") val par006 : Int = 0,
    @SerializedName("PAR_017") val par017 : Int = 0,
    @SerializedName("PAR_007") val par007 : Int = 0,
    @SerializedName("PAR_018") val par018 : String = "",
    @SerializedName("PAR_008") val par008 : Int = 0,
    @SerializedName("PAR_019") val par019 : Int = 0,
    @SerializedName("PAR_009") val par009 : Int = 0,
    @SerializedName("PAR_002") val par002 : String = "",
    @SerializedName("PAR_013") val par013 : Int = 0,
    @SerializedName("PAR_024") val par024 : String = "",
    @SerializedName("PAR_003") val par003 : String = "",
    @SerializedName("PAR_014") val par014 : Int = 0,
    @SerializedName("PAR_004") val par004 : String = "",
    @SerializedName("term") val term : String = "",
    @SerializedName("PAR_005") val par005 : Int = 0,
    @SerializedName("PAR_020") val par020 : Int = 0,
    @SerializedName("PAR_010") val par010 : Int = 0,
    @SerializedName("PAR_021") val par021 : Int = 0,
    @SerializedName("PAR_011") val par011 : String = "",
    @SerializedName("PAR_022") val par022 : Int = 0,
    @SerializedName("PAR_001") val par001 : String = "",
    @SerializedName("PAR_012") val par012 : String = "",
    @SerializedName("PAR_023") val par023 : String = "",
    @SerializedName("PAR_015") val par015 : String = "0",
    @SerializedName("PAR_025") val par025 : String = "0"
)