package com.example.unittest.screens.common.model

import com.valid.communication.helpers.CommunicationConstants

enum class MethodType(method: Int) {
    GET(CommunicationConstants.GET),
    POST(CommunicationConstants.POST),
    PUT(CommunicationConstants.PUT),
    DELETE(CommunicationConstants.DELETE)
}