package com.example.unittest.screens.otp.model

sealed class OtpCreationState
object OtpCreationSuccess : OtpCreationState()
data class OtpCreationError(val message: String = "", val code: Int = 0, val httpCode: Int = 0) :
    OtpCreationState()