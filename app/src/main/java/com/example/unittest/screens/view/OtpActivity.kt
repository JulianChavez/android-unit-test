package com.example.unittest.screens.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.unittest.R
import com.example.unittest.databinding.ActivityOtpBinding
import com.example.unittest.screens.common.base.BaseActivity
import com.example.unittest.screens.otp.viewmodel.OtpViewModel

class OtpActivity : BaseActivity() {
    lateinit var viewModel: OtpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityOtpBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_otp)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(OtpViewModel::class.java)
    }
}