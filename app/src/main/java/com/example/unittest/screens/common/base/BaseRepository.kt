package com.example.unittest.screens.common.base

import com.example.unittest.screens.common.model.VsshCommunicationRequest
import com.valid.communication.events.BaseErrorEvent
import com.valid.communication.events.BaseSuccessEvent
import com.valid.communication.managers.communicationmanager.CommunicationManager
import com.valid.communication.managers.communicationmanager.CommunicationManagerCallback
import com.valid.communication.models.BaseModelResponse

open class BaseRepository {

    lateinit var communicationManager: CommunicationManager


    fun <R: Any> consumeVsshService(request: VsshCommunicationRequest,
                                    responseType: Class<R>,
                                    response: (BaseModelResponse<in R>) -> Unit,
                                    error: (BaseErrorEvent) -> Unit,
                                    onHeadersResponse: ((Any) -> Unit)? = null) {

        this.communicationManager.setEnableRequestWithSSLPinning(request.sslPinning)
        this.communicationManager.isResponseEncrypted(request.responseEncrypted)
        this.communicationManager.disableSecureRequest(!request.secureRequest)
        this.communicationManager.addSessionIdInEncryptedData(request.addSessionIdToRequest)

        this.communicationManager.setCallback(object : CommunicationManagerCallback {
            override fun errorEvent(errorEvent: BaseErrorEvent?) {
                errorEvent?.let {
                    error(it)
                }
            }

            override fun successEvent(successEvent: BaseSuccessEvent?) {
                successEvent?.let {
                    onHeadersResponse?.invoke(successEvent.header)
                    response(successEvent.getBaseResponseModel(responseType))
                }
            }
        })

        this.communicationManager.consumeVsshService(request.method.ordinal, request.path, request.headers, request.body)
    }

}
