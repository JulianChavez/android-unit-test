package com.example.unittest.screens.main.repository

import android.content.Context
import com.example.unittest.BuildConfig
import com.example.unittest.screens.common.base.BaseRepository
import com.example.unittest.screens.common.model.EnumErrorType
import com.example.unittest.screens.common.model.ErrorModelResponse
import com.example.unittest.screens.common.model.ValidateUserModel
import com.example.unittest.screens.login.model.HeaderResponse
import com.example.unittest.utils.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.valid.communication.events.BaseErrorEvent
import com.valid.communication.events.BaseSuccessEvent
import com.valid.communication.helpers.CommunicationConstants
import com.valid.communication.managers.communicationmanager.CommunicationManagerCallback
import com.valid.communication.managers.communicationmanager.CommunicationManagerImp
import com.valid.communication.models.BaseModelResponse
import com.valid.communication.models.ExchangeRequestData
import com.valid.security.helpers.KeyHelper
import com.valid.utils.VsshLogger
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.exception.CommunicationException
import com.valid.vssh_android_core.model.ClientHelloModel
import com.valid.vssh_android_core.model.SetupModel
import com.valid.vssh_android_core.model.response.ClientHelloModelResponse
import com.valid.vssh_android_core.utils.VsshCoreConstants
import javax.inject.Inject
import javax.inject.Singleton

class MainRepository @Inject constructor(private val coreManager: VsshCoreManager) :
    BaseRepository(), CommunicationManagerCallback {

    private lateinit var isSetup: (value: Boolean) -> Unit
    private lateinit var mContext: Context

    fun setUp(mContext: Context, isSetup: (value: Boolean) -> Unit) {
        this.mContext = mContext
        this.isSetup = isSetup
        if (PreferencesHelper(mContext).stringKeyValidation.isEmpty()) {
            this.exSetup(mContext, isSetup)
        } else {
            this.getCertificates(mContext, isSetup)
        }
    }

    private fun exSetup(mContext: Context, isSetup: (value: Boolean) -> Unit) {
        val setupModel = SetupModel()
        setupModel.context = mContext
        setupModel.urlBase = BuildConfig.BASE_URL
        setupModel.apiGetCertificates = AppConstants.API_GET_CERTIFICATES
        setupModel.publicKey = BuildConfig.PUBLIC_KEY
        setupModel.apiExchange = AppConstants.API_EXCHANGE_KEY

        coreManager.setup(
            setupModel,
            {
                isSetup(it)
            },
            {
                isSetup(false)
            }
        )
    }

    private fun getCertificates(mContext: Context, isSetup: (value: Boolean) -> Unit) {
        communicationManager = CommunicationManagerImp(
            mContext,
            BuildConfig.BASE_URL
        )

        communicationManager.setEnableRequestWithSSLPinning(false)
        communicationManager.isResponseEncrypted(false)
        communicationManager.disableSecureRequest(false)
        communicationManager.addSessionIdInEncryptedData(false)
        communicationManager.setCallback(this)
        communicationManager.doGetCertificate(
            AppConstants.API_GET_CERTIFICATES,
            BuildConfig.PUBLIC_KEY
        )
    }

    fun clientHello(
        validateUserModel: ValidateUserModel,
        onSuccess: (value: ClientHelloModelResponse) -> Unit,
        onError: (value: ErrorModelResponse) -> Unit
    ) {
        val mHeaders = hashMapOf(
            AppConstants.KEY_CUSTOMER to AppConstants.VALUE_CUSTOMER_CLIENT_HELLO
        )

        val additionalData = mapOf(
            AppConstants.KEY_BIRTH_DATE to validateUserModel.birthDate,
            AppConstants.KEY_FIRE_BASE_TOKEN to ParamsHelper.fireBaseToken
        )

        val clientHelloModel = ClientHelloModel().apply {
            context = validateUserModel.context
            api = AppConstants.API_CLIENT_HELLO
            applicationCode = AppConstants.VALUE_APPLICATION_CODE
            applicationId = BuildConfig.APPLICATION_ID
            applicationVersion = BuildConfig.VERSION_NAME
            idType = validateUserModel.idType
            idData = validateUserModel.idData
            this.additionalData = additionalData
            headers = mHeaders
        }

        try {
            coreManager.clientHello(
                clientHelloModel,
                { response ->
                    onSuccess(response)
                },
                { generic ->
                    onError(ErrorModelResponse(EnumErrorType.CUSTOM, generic.message))
                },
                { error ->
                    onError(ErrorModelResponse(EnumErrorType.HTTP, error.errorMessage))
                }
            )
        } catch (e: CommunicationException) {
            onError(ErrorModelResponse(EnumErrorType.CUSTOM, e.message))
        }
    }

    private fun refresh() {
        val encrypt = SecurityHelper.encryptRSA(
            KeyHelper.getFingerPrint(mContext),
            BuildConfig.PUBLIC_KEY
        )

        val exAdditionalData: Map<String, Any> = mapOf(
            AppConstants.KEY_SIGN to encrypt,
            AppConstants.KEY_AES_KEY to AppConstants.VALUE_AES_KEY
        )
        val exHeaders: Map<String, String> =
            mapOf(AppConstants.KEY_VALIDATION to PreferencesHelper(mContext).stringKeyValidation)

        val exchangeRequestData = ExchangeRequestData().apply {
            publicKey = BuildConfig.PUBLIC_KEY

        }

        communicationManager.setEnableRequestWithSSLPinning(false)
        communicationManager.isResponseEncrypted(false)
        communicationManager.disableSecureRequest(false)
        communicationManager.addSessionIdInEncryptedData(false)
        communicationManager.setCallback(this)
        communicationManager.initExchangeKey(
            AppConstants.API_AUTHENTICATION_KEY,
            AppConstants.EP_REFRESH_SESSION,
            exchangeRequestData,
            exAdditionalData,
            exHeaders
        )
    }

    fun getFireBaseToken(token: (value: String?) -> Unit) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    VsshLogger.logError(
                        MainRepository::class.java.simpleName,
                        "getInstanceId failed",
                        task.exception
                    )
                    token(null)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                token.invoke(task.result?.token)
            })
    }

    override fun errorEvent(baseErrorEvent: BaseErrorEvent?) {
        if (baseErrorEvent != null && baseErrorEvent.errorData != null) {
            if (AppConstants.SN_REFRESH == baseErrorEvent.errorData.serviceName && AppConstants.SC_EXPIRED_SESSION == baseErrorEvent.errorData.errorType) {
                sessionExpired(mContext)
                this.exSetup(mContext, isSetup)
            } else {
                this.isSetup(false)
            }
        } else {
            this.isSetup(false)
        }
    }

    override fun successEvent(baseSuccessEvent: BaseSuccessEvent?) {
        if (
            baseSuccessEvent == null ||
            baseSuccessEvent.response == null ||
            baseSuccessEvent.response.isEmpty()
        ) {
            this.isSetup(false)
            return
        }
        val typedResponse = baseSuccessEvent.getTypedResponse(BaseModelResponse::class.java)
        if (CommunicationConstants.STATUS_CODE_SUCCESS == typedResponse.statusCode) {
            when (typedResponse.serviceName) {
                VsshCoreConstants.SN_GET_CERTIFICATES -> refresh()
                AppConstants.SN_REFRESH, CommunicationConstants.SN_EXCHANGE_KEY -> {
                    coreManager.communicationManager = communicationManager
                    this.isSetup(true)

                    ParamsHelper.mapToken=
                        Gson().fromJson(baseSuccessEvent.header.toString(), HeaderResponse::class.java)
                            .crossAuthorization

                }
                else -> this.isSetup(false)
            }
        } else {
            this.isSetup(false)
        }
    }
}
