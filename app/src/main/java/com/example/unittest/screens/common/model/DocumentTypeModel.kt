package com.example.unittest.screens.common.model

import com.google.gson.annotations.SerializedName

data class DocumentTypeModel(
    @SerializedName("idType")
    var idType: String = "0",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("abbreviation")
    var abbreviation: String = ""
)