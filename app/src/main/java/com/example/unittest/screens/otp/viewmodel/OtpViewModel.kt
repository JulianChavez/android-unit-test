package com.example.unittest.screens.otp.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.unittest.screens.common.base.BaseViewModel
import com.example.unittest.screens.login.model.ValidateOtpRequestModel
import com.example.unittest.screens.otp.model.*
import com.example.unittest.screens.otp.repository.OtpRepository
import com.example.unittest.utils.AppConstants
import com.example.unittest.utils.ParamsHelper
import com.example.unittest.utils.ValidationUtils
import javax.inject.Inject

class OtpViewModel @Inject constructor(
    private val otpRepository: OtpRepository
) : BaseViewModel() {
    private val _otpValidationViewType = MutableLiveData<OtpValidationViewType>()
    val otpValidationViewType: LiveData<OtpValidationViewType> get() = _otpValidationViewType

    private val _device = MutableLiveData<Boolean>(false)
    val device: LiveData<Boolean> = _device

    private val _generateOtpResponse = MutableLiveData<OtpCreationState>()
    val generateOtpResponse: LiveData<OtpCreationState> get() = _generateOtpResponse

    private val _validateOtpResponse = MutableLiveData<OtpValidationState>()
    val validateOtpResponse: LiveData<OtpValidationState> get() = _validateOtpResponse

    private val _otpValue = MutableLiveData<String>()
    val otpValue: LiveData<String> get() = _otpValue

    var isThereOtp = MutableLiveData<Boolean>(false)
    var enableContinueButton = MutableLiveData<Boolean>(false)


    var otpIsNotGenerated: Int = 0

    private val _codeValidated = MutableLiveData(false)
    val codeValidated: LiveData<Boolean> get() = _codeValidated

    private val _enableBtnEmailPhoneConfirm = MutableLiveData(false)
    val enableButton: LiveData<Boolean> get() = _enableBtnEmailPhoneConfirm

    private val _failedGenerate = MutableLiveData(false)
    val failedGenerate: LiveData<Boolean> = _failedGenerate

    private val _typeError = MutableLiveData<PhoneMailErrorState>()
    val typeError: LiveData<PhoneMailErrorState> get() = _typeError

    val isPhone = MutableLiveData(false)
    val onIsPhone: LiveData<Boolean> get() = isPhone

    val endTime = MutableLiveData(false)
    val onEndTime: LiveData<Boolean> get() = endTime

    val showEditMail = MutableLiveData(false)
    val showModMail: LiveData<Boolean> get() = showEditMail

    val showEditPhone = MutableLiveData(false)
    val showModPhone: LiveData<Boolean> get() = showEditPhone

    var useAllies: Boolean = false

    private val _typedUserData = MutableLiveData("")

    lateinit var otpValidateType: String

    lateinit var viewData: String

    var idType: String = ""
    var idData: String = ""

    fun clearOtpLiveDataValues() {
        _generateOtpResponse.value = null
        _validateOtpResponse.value = null
        _failedGenerate.value = false
        showEditPhone.value = false
        endTime.value = false
        setCodeValidated(false)
    }

    fun clearPhoneMailConfirmationDataValues() {
        _typedUserData.value = ""
        _enableBtnEmailPhoneConfirm.value = false
        _typeError.value = null
    }

    fun setCodeValidated(validated: Boolean) {
        _codeValidated.value = validated
    }

    fun setViewUsername(): String? = if(!::viewData.isInitialized) null else viewData

    private fun generateOtp(email: String = "", cellPhone: String = "") {
        otpRepository.generateOtp(
            phone = cellPhone,
            mail = email,
            onSuccess = { response, otp ->
                isThereOtp.value = true
                _generateOtpResponse.value = response
                _otpValue.value = otp
                _failedGenerate.value = false
            },
            onError = { message ->
                if (message.code == AppConstants.SC_EXPIRED_SESSION || message.code == AppConstants.SC_EXPIRED_SESSION_CORE){
                    errorSessionExpired.value = true
                }else{
                    _generateOtpResponse.value = message
                    _failedGenerate.value = true
                }

            })
    }

    fun validateOtp(context: Context? = null, value: String) {
        showLoading.set(true)

        val request = ValidateOtpRequestModel(
            context = context,
            idType = idType,
            idData = idData,
            otpValue = value,
            isPhone = onIsPhone.value!!,
            allies = useAllies
        )

        otpRepository.validateOtp(
            request = request,
            onSuccess = { response ->
                otpIsNotGenerated = 0
                showLoading.set(false)
                setCodeValidated(true)
                _validateOtpResponse.value = response
            },
            onError = { message ->
                showLoading.set(false)
                if (message.code == AppConstants.SC_EXPIRED_SESSION || message.code == AppConstants.SC_EXPIRED_SESSION_CORE) {
                        errorSessionExpired.value = true
                } else {
                    _validateOtpResponse.value = message
                }
            }
        )
    }

    fun callGenerateOtp() {
        if (!::otpValidateType.isInitialized || codeValidated.value == true) return
        if (onIsPhone.value == true)
            generateOtp(cellPhone = otpValidateType)
        else
            generateOtp(email = otpValidateType)
    }

    fun validateConfirmationTypedText(data: String) {
        _typedUserData.value = data
        if (onIsPhone.value == true)
            _enableBtnEmailPhoneConfirm.value = ValidationUtils.validatePhoneNumber(data)
        else
            _enableBtnEmailPhoneConfirm.value = !ValidationUtils.isEmptyOrBlankText(data)
                    && ValidationUtils.validateEmailAddress(data)
    }

    fun validateErrorState(): Boolean {
        if (_typedUserData.value != otpValidateType) {
            if (onIsPhone.value == true)
                _typeError.value = PhoneErrorState
            else
                _typeError.value = MailErrorState
            return false
        }
        return true
    }

    fun setViewType(viewState: OtpValidationViewType) {
        _otpValidationViewType.value = viewState
        _device.value = viewState == OtpValidationViewType.NEW_DEVICE
    }

    fun setFailedGenerate(failed: Boolean) {
        _failedGenerate.value = failed
    }

    fun enableButton(otpCode: String) {
        enableContinueButton.value = isThereOtp.value!! && otpCode.length == ParamsHelper.params.recoverCodeLength
    }

} 