package com.example.unittest.di.module

import com.example.unittest.screens.login.view.LoginActivity
import com.example.unittest.screens.main.view.MainActivity
import com.example.unittest.screens.splash.view.SplashActivity
import com.example.unittest.screens.view.OtpActivity
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract interface ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun bindOtpActivity(): OtpActivity
}
