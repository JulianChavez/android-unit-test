package com.example.unittest.di.module

import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.VsshCoreManagerImp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CoreModule {

    @Singleton
    @Provides
    fun provideCoreHelper(): VsshCoreManager {
        return VsshCoreManagerImp()
    }
}