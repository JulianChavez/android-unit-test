package com.example.unittest.di.module

import androidx.lifecycle.ViewModel
import com.example.unittest.di.annotation.ViewModelKey
import com.example.unittest.screens.login.viewmodel.LoginViewModel
import com.example.unittest.screens.main.viewmodel.MainViewModel
import com.example.unittest.screens.splash.viewmodel.SplashViewModel
import com.example.unittest.screens.otp.viewmodel.OtpViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OtpViewModel::class)
    fun bindOtpViewModel(viewModel: OtpViewModel): ViewModel
}
