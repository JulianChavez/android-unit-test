package com.example.unittest

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.unittest.TestUtils.anyObject
import com.example.unittest.screens.common.model.*
import com.example.unittest.screens.login.repository.LoginRepository
import com.example.unittest.screens.login.viewmodel.LoginViewModel
import com.example.unittest.screens.main.repository.MainRepository
import com.valid.vssh_android_core.model.response.ClientHelloModelResponse
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

private const val HTTP_ERROR_MESSAGE = "400"

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {
    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    //Test subject
    private lateinit var loginViewModel: LoginViewModel
    //Mock objects
    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var loginRepository: LoginRepository


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        //Inject mocks
        loginViewModel = LoginViewModel(
            loginRepository,
            mainRepository
        )

    }

    @Test
    fun `check if the clientHello method is executed if the setUp is correct`() {

        `when`(
            mainRepository.setUp(
                anyObject(),
                anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(Boolean) -> Unit>(1).invoke(true)
        }

        loginViewModel.validateUser(ValidateUserModel(context, "", "", ""))

        verify(mainRepository, times(1)).clientHello(anyObject(), anyObject(), anyObject())
    }

    @Test
    fun `users are not validated without initial configuration`() {

        `when`(
            mainRepository.setUp(
                anyObject(),
                anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(Boolean) -> Unit>(1).invoke(false)
        }

        loginViewModel.validateUser(ValidateUserModel(context, "", "", ""))

        verify(mainRepository, never()).clientHello(anyObject(), anyObject(), anyObject())
    }

    @Test
    fun `when the clientHello response is successful it should be propagated as is`() {
        val clientHelloModelResponse = ClientHelloModelResponse()
        clientHelloModelResponse.allowLoginOnUnassignedDevice = true
        clientHelloModelResponse.deviceAssigned = false
        clientHelloModelResponse.userExists = true
        clientHelloModelResponse.userState = 0


        `when`(
            mainRepository.setUp(
                anyObject(),
                anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(Boolean) -> Unit>(1).invoke(true)
        }

        `when`(
            mainRepository.clientHello(
                anyObject(),
                anyObject(),
                anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(ClientHelloModelResponse) -> Unit>(1).invoke(clientHelloModelResponse)
        }

        loginViewModel.validateUser(ValidateUserModel(context, "", "", ""))

        assertEquals(clientHelloModelResponse, loginViewModel.responseClientHello.value)
    }

    @Test
    fun `clientHello service response error`() {

        val errorModelResponse = ErrorModelResponse()
        errorModelResponse.errorMessage = HTTP_ERROR_MESSAGE

        errorModelResponse.errorType = EnumErrorType.CUSTOM

        `when`(
            mainRepository.setUp(
                anyObject(),
                anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(Boolean) -> Unit>(1).invoke(true)
        }

        `when`(
            mainRepository.clientHello(
                anyObject(),
                anyObject(),
                anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(ErrorModelResponse) -> Unit>(2).invoke(errorModelResponse)
        }

        loginViewModel.validateUser(ValidateUserModel(context, "", "",""))

        assertEquals(errorModelResponse, loginViewModel.clientHelloError.value)

    }


}

