package com.example.unittest

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.unittest.screens.otp.model.*
import com.example.unittest.screens.otp.repository.OtpRepository
import com.example.unittest.screens.otp.viewmodel.OtpViewModel
import com.example.unittest.utils.AppConstants
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class OtpValidationViewModelTest {

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()


    //Test subject
    private lateinit var otpViewModel: OtpViewModel

    //Mock objects
    @Mock
    private lateinit var otpRepository: OtpRepository

    @Mock
    private lateinit var context: Context

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        //Inject mock
        otpViewModel = OtpViewModel(otpRepository)
    }

    @Test
    fun `check if the generateOtp method is called`() {
        otpViewModel.isPhone.value = true
        otpViewModel.otpValidateType = "3106598745"
        otpViewModel.callGenerateOtp()

        verify(otpRepository, times(1)).generateOtp(
            TestUtils.anyObject(),
            TestUtils.anyObject(),
            TestUtils.anyObject(),
            TestUtils.anyObject()
        )
    }

    @Test
    fun `otp code is successfully generated and validated`() {
        val otpCreationSuccess = OtpCreationSuccess
        val otpCodeMock = "123456"

        `when`(
            otpRepository.generateOtp(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(OtpCreationState, String) -> Unit>(2).invoke(otpCreationSuccess, otpCodeMock)
        }

        otpViewModel.isPhone.value = true
        otpViewModel.otpValidateType = ""
        otpViewModel.callGenerateOtp()

        assertEquals(otpCreationSuccess, otpViewModel.generateOtpResponse.value)
        assertTrue(otpCodeMock == otpViewModel.otpValue.value)
    }

    @Test
    fun `verify failed otp generation`() {
        val otpCreationError = OtpCreationError()
        `when`(
            otpRepository.generateOtp(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(OtpCreationState) -> Unit>(3).invoke(otpCreationError)
        }

        otpViewModel.isPhone.value = true
        otpViewModel.otpValidateType = ""
        otpViewModel.callGenerateOtp()

        assertEquals(otpCreationError, otpViewModel.generateOtpResponse.value)
        assertTrue(otpViewModel.failedGenerate.value!!)
    }

    @Test
    fun `corroborate the otp validation`() {
        val otpValidationSuccess = OtpValidationSuccess("asfsdgfdh41g36j8h6g3j4164k6i634646413541g")
        val otpCodeMock = "987654"

        `when`(
            otpRepository.validateOtp(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(OtpValidationSuccess) -> Unit>(1).invoke(otpValidationSuccess)
        }
        otpViewModel.validateOtp(context, otpCodeMock)

        assertEquals(otpValidationSuccess, otpViewModel.validateOtpResponse.value)
    }

    @Test
    fun `check if the otp error is because the session expired`() {
        val otpValidationError = OtpValidationError("", AppConstants.SC_EXPIRED_SESSION)
        val otpCodeMock = "987654"

        `when`(
            otpRepository.validateOtp(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(OtpValidationError) -> Unit>(2).invoke(otpValidationError)
        }

        otpViewModel.validateOtp(context, otpCodeMock)

        assert(otpViewModel.errorSessionExpired.value!!)
    }

}