package com.example.unittest.screens.login.view

import android.os.Build
import android.widget.Button
import android.widget.ImageView
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.unittest.R
import com.google.android.material.textfield.TextInputLayout
import junit.framework.TestCase.assertTrue
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.annotation.Config


@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    private lateinit var activity: LoginActivity

    @Before
    fun setUp() {
        activity = Robolectric.setupActivity(LoginActivity::class.java)
    }

    @Test
    fun `verify dimensions of credentials fragment image`() {

        val imageDimensions = "300"
        val imageCredentialsFragment = activity.findViewById(R.id.iv_welcome) as ImageView
        val height = imageCredentialsFragment.height.toString()
        val width = imageCredentialsFragment.width.toString()

        assertTrue(
            "The image does not meet the required height",
            imageDimensions == height
        )

        assertTrue(
            "The image does not meet the required width",
            imageDimensions == width
        )
    }

    @Test
    fun `confirm the hint of textInputLayout`() {

        val hintDocument = "Ingresa tu cedula"
        val hintDate = "Fecha de nacimiento"
        val tiDocument = activity.findViewById(R.id.ti_document) as TextInputLayout
        val tiDate = activity.findViewById(R.id.ti_date) as TextInputLayout
        assertNotNull("The textInputLayout was not found", tiDocument)
        assertNotNull("The textInputLayout was not found", tiDate)
        assertTrue(
            "The hint is not suitable",
            hintDocument == tiDocument.hint.toString()
        )

        assertTrue(
            "The hint is not required",
            hintDate == tiDate.hint.toString()
        )
    }

    @Test
    fun `check button text continue`() {

        val realText = "Continuar"
        val btnContinue = activity.findViewById(R.id.btn_continue) as Button
        assertNotNull("The button was not found", btnContinue)
        assertTrue(
            "The button does not have the correct text",
            realText == btnContinue.text.toString()
        )
    }

}