package com.example.unittest.screens.login.view

import android.os.Build
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.unittest.R
import org.hamcrest.core.IsNot.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config


@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
class LoginActivityEspressoTest {
    @Rule
    @JvmField
    var activityScenarioRule: ActivityScenarioRule<LoginActivity> =
        ActivityScenarioRule<LoginActivity>(
            LoginActivity::class.java
        )


    @Test
    fun `verify that initial elements are visible to the user`() {

        onView(withId(R.id.iv_welcome))
            .check(matches(isDisplayed()))

        onView(withId(R.id.ti_document))
            .check(matches(isDisplayed()))

        onView(withId(R.id.ti_document))
            .check(matches(isFocusable()))

        onView(withId(R.id.ti_document))
            .check(matches(isFocused()))

    }

    @Test
    fun `configure input data to validate user and test continue button`() {

        onView(withId(R.id.et_document))
            .perform(typeText("9006337"), closeSoftKeyboard())

        onView(withId(R.id.et_date))
            .perform(scrollTo(), typeText("03/03/1948"), closeSoftKeyboard())

        onView(withId(R.id.btn_continue))
            .check(matches(not(isSelected())))
        onView(withId(R.id.btn_continue))
            .check(matches(isEnabled()))
        onView(withId(R.id.btn_continue))
            .check(matches(withText(R.string.txt_continue)))

        onView(withId(R.id.btn_continue))
            .perform(scrollTo(), click())
    }



    @Test
    fun `validate that no HomeFragment view appears in the initial view`() {

        onView(withId(R.id.image_user_data))
            .check(doesNotExist())

        onView(withId(R.id.tv_profile_title))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_name))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_name_title))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_address))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_address_title))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_cellphone))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_cellphone_title))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_email))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_email_title))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_profession))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_profession_title))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_company))
            .check(doesNotExist())


        onView(withId(R.id.tv_user_company_title))
            .check(doesNotExist())


    }


}