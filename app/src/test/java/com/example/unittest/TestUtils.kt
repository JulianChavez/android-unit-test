package com.example.unittest

import org.mockito.Mockito
import kotlin.random.Random

object TestUtils {

    fun <T> anyObject(): T {
        Mockito.anyObject<T>()
        return uninitialized()
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

}